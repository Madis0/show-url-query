# Show URL query

![](https://i.imgur.com/Uwr0K4z.png)

Shows the query and anchor tags as a PageAction, if they exist. You can either hover or click the button to see it.

Made for Vivaldi because I want to keep the protocol hidden but still be aware of any queries.

[Download here](https://gitlab.com/Madis0/show-url-query/-/tags) - drag the crx to `vivaldi://extensions`

---

Not going to pay Google to upload this to Chrome Web Store, also [this is incompatible with Manifest v3](https://developer.chrome.com/extensions/migrating_to_manifest_v3#api_checklist) by nature, so enjoy it while you can.

Based on https://developer.chrome.com/extensions/samples#page-action-by-url

Icon from https://materialdesignicons.com/icon/bullseye